<?php

return [
    'active' => 'Active',
    'not_active' => 'Not active',
    'finished' => 'Finished',
    'welcome' => 'Welcome to our application',
    'empty_result' => 'No results found.',
    'no_set' => 'No set.',
    'want_to_delete_item' => 'Are you sure you want to delete this item?',
    'show' => 'Show',
    'create' => 'Create',
    'create_obj' => 'Create :name',
    'update' => 'Update',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'destroy' => 'Destroy',
    '(not set)' => '(not set)',
    'small' => 'Small',
    'big' => 'Big',
    'director' => 'Director',
    'actor' => 'Actor',
];