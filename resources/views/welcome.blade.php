<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ URL::asset('css/markdown.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/beautify-json.css') }}" />
            <style>
                .markdown-body {
                    box-sizing: border-box;
                    min-width: 200px;
                    max-width: 980px;
                    margin: 0 auto;
                    padding: 45px;
                }

                @media (max-width: 767px) {
                    .markdown-body {
                        padding: 15px;
                    }
                }

                
        </style>
        <script type="text/javascript" src="{{ URL::asset('js/jquery-1.12.2.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.beautify-json.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.language-json').beautifyJSON(); 
            });
        </script>
       
    </head>
    <body>
            <div class="markdown-body container">
                {!! $docs !!}
            </div>
        </div>        

    </body>
</html>
