# REST API BACKEND

 Base URL: {{ env('APP_URL') }}
- [Login](#login)
- [Play](#play)
  - [Index Play](#index-play)
  - [Create Play](#create-play)
  - [Update Play](#update-play)
  - [Show Play](#show-play)
  - [Delete Play](#delete-play)
- [Person](#person)
  - [Index Person](#index-person)
  - [Create Person](#create-person)
  - [Update Person](#update-person)
  - [Show Person](#show-person)
  - [Delete Person](#delete-person)
- [News](#news)
  - [Index News](#index-news)
  - [Create News](#create-news)
  - [Update News](#update-news)
  - [Show News](#show-news)
  - [Delete News](#delete-news)
- [Gallery](#gallery)
  - [Index News](#index-gallery)
  - [Create Gallery](#create-gallery)
  - [Update Gallery](#update-gallery)
  - [Show Gallery](#show-gallery)
  - [Delete Gallery](#delete-gallery)
- [GalleryCategory](#gallery-category)
  - [Index News](#index-gallery-category)
  - [Create Gallery](#create-gallery-category)
  - [Update Gallery](#update-gallery-category)
  - [Show Gallery](#show-gallery-category)
  - [Delete Gallery](#delete-gallery-category)

# REST API FRONTEND
- [Play](#play-front)
  - [Index Play](#index-play-front)
  - [Show Play](#show-play-front)
- [Person](#person-front)
  - [Index Person](#index-person-front)
  - [Show Person](#show-person-front)
- [News](#news-front)
  - [Index News](#index-news-front)
  - [Show Play](#show-news-front)
- [Gallery](#gallery-front)
  - [Index Gallery](#index-gallery-front)
  - [Show Gallery](#show-gallery-front)
- [Search](#search)
  

## <a name="login"></a>Login

Для ипсользования всех запросов админки, пользователем должен быть администратором.
После авторизации по email и password мы имеем `token` Bearer  который нужно использовать для заголовка Authorization.
Пример использования:
`Authorization: Bearer SR8Ivs8ztHK15ht7FZdIQtez0U7ywsutKorfOm18Z6dv5qbtF7DJku6Rnfdf`

Логирование:

`{{env('APP_URL')}}/api/v1/backend/login` [POST]

Response (Status Code: 200)

```json
{
    "token": "SR8Ivs8ztHK15ht7FZdIQtez0U7ywsutKorfOm18Z6dv5qbtF7DJku6Rnfdf"
}
```


## <a name="play"></a>Play
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/backend/play/?expand=personPhotos,plays,scenario мы получим информацию по спектаклю,
которая будет содержать дополнительно данные про фото и актеров, сценариев. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-play"></a> Index Play

`{{env('APP_URL')}}/api/v1/backend/play` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 3,
            "name": "Walker, Berge and Herman",           
            "description": "Voluptates dolorem est porro itaque. Magnam qui aut repudiandae. Necessitatibus tempore ut et reprehenderit vero. Quasi ex quia et iusto facere soluta iste et.",
            "date": "1975-01-05 13:55:46",
            "status": 2,
            "image": "https://lorempixel.com/gray/320/240/cats/Faker/?18083",
            "image_path": "320/240/cats/Faker/?18083",
            "created_at": "2019-03-08 13:19:28",
            "updated_at": "2019-03-08 13:19:28"
        }       
    ],
    "links": {
        "first": "http://theater.loc/api/v1/backend/play?page=1",
        "last": "http://theater.loc/api/v1/backend/play?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/backend/play",
        "per_page": 15,
        "to": 5,
        "total": 5
    }
}
```


## <a name="create-play"></a> Create Play

`{{env('APP_URL')}}/api/v1/backend/play` [POST]

Fields:

```
name = String, NOT NULL
genre = String, NOT NULL (Потом удалю, когда будет материал)
description = String, NOT NULL
date = Date Time Format(Y-m-d H:i:s), NOT NULL,
duration = Integer, NOT NULL (продолжительность спектакля в минутах)
status = Integer, NOT NULL,
play_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
scenario = File, NULL, extensions (doc, docx, pdf)
play_photos = File, Array, Null (Example: play_photos[0] = File, play_photos[1] = File)

```

Response (Status Code: 201)

```json
{
    "id": 10,
    "name": "Margot Robbie",
    "genre": "Wolf of Wall Street",
    "duration": "90",
    "description": "Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi. Alias doloribus nulla architecto et deserunt. Occaecati delectus soluta nihil optio sint aliquid rem. Praesentium omnis sunt consectetur a. Sit consequatur beatae sequi commodi.",
    "date": "2019-03-10 19:00:00",
    "status": "2",
    "image": "",
    "image_path": null,
    "created_at": "2019-03-12 15:50:36",
    "updated_at": "2019-03-12 15:50:36"
}
```

## <a name="update-play"></a> Update Play

`{{env('APP_URL')}}/api/v1/backend/play/666` [PUT] 

Fields:

```
name = String, NOT NULL
genre = String, NOT NULL (Потом удалю, когда будет материал)
description = String, NOT NULL
date = Date Time Format(Y-m-d H:i:s), NOT NULL,
duration = Integer, NOT NULL (продолжительность спектакля в минутах)
status = Integer, NOT NULL,
play_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
scenario = File, NULL, extensions (doc, docx, pdf) (Перед тем как еще раз добавить докумен, обязательно старый добавь в delete_files, иначе всегда будет отображаться 1й документ)
play_photos = File, Array, Null (Example: play_photos[0] = File, play_photos[1] = File)
delete_files = String Array, Null (Example: delete_files[0] = source/Yjnro9004KbNjfM4WpGBrFDdbyikYfrc3q4PROeN.jpeg) Указывается название файла, он приходит как атрибут image_path и scenario_path
```

Response (Status Code: 200)

```json
{
    "id": 666,
    "name": "Don Simon",
    "genre": "деревенский сериал",
    "duration": "75",
    "description": "Alias doloribus nulla architecto et deserunt",
    "date": "2019-03-10 19:00:00",
    "status": "2",
    "image": "http://theater.loc/storage/source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "image_path": "source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "created_at": "2019-03-12 15:50:36",
    "updated_at": "2019-03-12 16:00:19",
    "playPhotos": [
        {
            "image": "http://theater.loc/storage/source/BFGnInltzFSRgfLZc3mczB7kquuD31IAp1vr9jz6.jpeg",
            "image_path": "source/BFGnInltzFSRgfLZc3mczB7kquuD31IAp1vr9jz6.jpeg"
        }
    ]
}
```

## <a name="show-play"></a> Show Play

`{{env('APP_URL')}}/api/v1/backend/play/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "name": "Don Simon",
    "genre": "деревенский сериал",
    "duration": "75",
    "description": "Alias doloribus nulla architecto et deserunt",
    "date": "2019-03-10 19:00:00",
    "status": "2",
    "image": "http://theater.loc/storage/source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "image_path": "source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "created_at": "2019-03-12 15:50:36",
    "updated_at": "2019-03-12 16:00:19"    
}
``` 

## <a name="delete-play"></a> Delete Play

`{{env('APP_URL')}}/api/v1/backend/play/666` [DELETE] 

Response (Status Code: 204)

## <a name="person"></a>Person
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/backend/person/?expand=playPhotos,persons мы получим информацию об учасниках спектакля,
которая будет содержать дополнительно данные про фото и где актеры играли. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-person"></a> Index Person

`{{env('APP_URL')}}/api/v1/backend/person` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "first_name": "Nona",
            "last_name": "Dicki",
            "description": "Molestiae sit quas voluptates dolorem consectetur temporibus autem. Harum dolores quo molestiae laudantium. Quos dolorem unde voluptate sapiente. Eum fuga quasi beatae vel.",
            "status": 2,
            "image": "https://lorempixel.com/gray/320/240/people/Faker/?44159",
            "image_path": "320/240/people/Faker/?44159",
            "created_at": "2019-03-08 13:19:46",
            "updated_at": "2019-03-08 13:19:46"
        },
        {
            "id": 2,
            "first_name": "Dorris",
            "last_name": "Nolan",
            "description": "Aliquid cumque voluptate dolor velit dolorum est necessitatibus. Ea qui ut qui ad. Nihil facere tenetur quis maiores. Saepe earum distinctio ipsum minus saepe ratione eius.",
            "status": 2,
            "image": "https://lorempixel.com/gray/320/240/people/Faker/?19242",
            "image_path": "320/240/people/Faker/?19242",
            "created_at": "2019-03-08 13:19:46",
            "updated_at": "2019-03-08 13:19:46"
        }
    ],
    "links": {
        "first": "http://theater.loc/api/v1/backend/person?page=1",
        "last": "http://theater.loc/api/v1/backend/person?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/backend/person",
        "per_page": 15,
        "to": 13,
        "total": 13
    }
}
```

## <a name="create-person"></a> Create Person

`{{env('APP_URL')}}/api/v1/backend/person` [POST]

Fields:

```
first_name = String, NOT NULL
last_name = String, NOT NULL
description = String, NOT NULL
status = Integer, NOT NULL,
person_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
person_photos = File, Array, Null (Example: person_photos[0] = File, person_photos[1] = File)
```
Response (Status Code: 201)

```json
{
    "id": 15,
    "first_name": "Jack",
    "last_name": "Black",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam.",
    "status": "2",
    "image": "http://theater.loc/storage/source/lyL11GY4NRRavPjo0ALptC2I75x1qfzwSSi3zrHk.jpeg",
    "image_path": "source/lyL11GY4NRRavPjo0ALptC2I75x1qfzwSSi3zrHk.jpeg",
    "created_at": "2019-03-18 18:09:45",
    "updated_at": "2019-03-18 18:09:45"
}
```

## <a name="update-person"></a> Update Person

`{{env('APP_URL')}}/api/v1/backend/person/666` [PUT] 


Fields:

```
first_name = String, NOT NULL
last_name = String, NOT NULL
description = String, NOT NULL
status = Integer, NOT NULL,
person_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
person_photos = File, Array, Null (Example: person_photos[0] = File, person_photos[1] = File)
delete_files = String Array, Null (Example: delete_files[0] = source/Yjnro9004KbNjfM4WpGBrFDdbyikYfrc3q4PROeN.jpeg) Указывается название файла, он приходит как атрибут image_path
```

Response (Status Code: 200)

```json
{
    "id": 666,
    "first_name": "Jack",
    "last_name": "Black",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam.",
    "status": "2",
    "image": "http://theater.loc/storage/source/AY4TTspUIMNowHQUfvUHzVH3rrwfujBIeNqQf5yJ.jpeg",
    "image_path": "source/AY4TTspUIMNowHQUfvUHzVH3rrwfujBIeNqQf5yJ.jpeg",
    "created_at": "2019-03-18 18:09:45",
    "updated_at": "2019-03-18 18:16:28",
    "personPhotos": [
        {
            "image": "http://theater.loc/storage/source/lpBSUfFMYIufDLzMyARwDM2ksngZXhPn6tuYXfs9.jpeg",
            "image_path": "source/lpBSUfFMYIufDLzMyARwDM2ksngZXhPn6tuYXfs9.jpeg"
        },
        {
            "image": "http://theater.loc/storage/source/fUmAnRnFVi94kGtDkbXA2JVXXvCxfNnzAhL2e4MT.jpeg",
            "image_path": "source/fUmAnRnFVi94kGtDkbXA2JVXXvCxfNnzAhL2e4MT.jpeg"
        }
    ],
    "plays": [
        {
            "id": 24,
            "play_id": 10,
            "person_id": 666,
            "name": "Kyla Sporer",
            "type": 2
        }
    ]
}
```

## <a name="show-person"></a> Show Person

`{{env('APP_URL')}}/api/v1/backend/person/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 15,
    "first_name": "Jack",
    "last_name": "Black",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam.",
    "status": 2,
    "image": "http://theater.loc/storage/source/AY4TTspUIMNowHQUfvUHzVH3rrwfujBIeNqQf5yJ.jpeg",
    "image_path": "source/AY4TTspUIMNowHQUfvUHzVH3rrwfujBIeNqQf5yJ.jpeg",
    "created_at": "2019-03-18 18:09:45",
    "updated_at": "2019-03-18 18:16:28"
}
```

## <a name="delete-person"></a> Delete Person

`{{env('APP_URL')}}/api/v1/backend/person/666` [DELETE] 

Response (Status Code: 204)



## <a name="news"></a>News
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/backend/news/?expand=newsPhotos мы получим информацию об новостях,
которая будет содержать дополнительно данные про фото. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-news"></a> Index News

`{{env('APP_URL')}}/api/v1/backend/news` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "title": "Sint quis autem earum.",
            "description": "Autem placeat atque nobis quas dolorem enim et. Quos qui autem enim quia exercitationem aut. Incidunt itaque eius et molestiae. Deleniti sed quod earum omnis. Voluptas quibusdam dignissimos ut.",
            "date": "2004-11-01",
            "status": 2,
            "image": "https://lorempixel.com/320/240/cats/Faker/?34848",
            "image_path": "320/240/cats/Faker/?34848",
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59",
            "newsPhotos": [
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?66906",
                    "image_path": "460/380/cats/Faker/?66906"
                },
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?52987",
                    "image_path": "460/380/cats/Faker/?52987"
                }
            ]
        },
        {
            "id": 2,
            "title": "Tempora minus est.",
            "description": "Nisi eum nihil reprehenderit et dolorem. Autem nulla iusto aspernatur aspernatur earum voluptas eum.",
            "date": "1990-11-15",
            "status": 2,
            "image": "https://lorempixel.com/320/240/cats/Faker/?51123",
            "image_path": "320/240/cats/Faker/?51123",
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59",
            "newsPhotos": [
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?52081",
                    "image_path": "460/380/cats/Faker/?52081"
                },
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?24266",
                    "image_path": "460/380/cats/Faker/?24266"
                }
            ]
        }
    ],
    "links": {
        "first": "http://theater.loc/api/v1/backend/news?page=1",
        "last": "http://theater.loc/api/v1/backend/news?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/backend/news",
        "per_page": 15,
        "to": 8,
        "total": 8
    }
}
```


## <a name="create-news"></a> Create News

`{{env('APP_URL')}}/api/v1/backend/news` [POST]

Fields:

```
title = String, NOT NULL
description = String, NOT NULL
date = Date Format(Y-m-d), NOT NULL,
status = Integer, NOT NULL,
news_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
news_photos = File, Array, Null (Example: news_photos[0] = File, news_photos[1] = File)
```

Response (Status Code: 201)

```json
{
    "id": 11,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "status": "2",
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```


## <a name="update-news"></a> Update News

`{{env('APP_URL')}}/api/v1/backend/news/666` [PUT] 

Fields:

```
title = String, NOT NULL
description = String, NOT NULL
date = Date Format(Y-m-d), NOT NULL,
status = Integer, NOT NULL,
news_photo = File, NULL, extensions (jpeg, jpg, bmp, png)
news_photos = File, Array, Null (Example: news_photos[0] = File, news_photos[1] = File)
delete_files = String Array, Null (Example: delete_files[0] = source/Yjnro9004KbNjfM4WpGBrFDdbyikYfrc3q4PROeN.jpeg) Указывается название файла, он приходит как атрибут image_path
```

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "status": "2",
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="show-news"></a> Show News

`{{env('APP_URL')}}/api/v1/backend/news/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "status": "2",
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="delete-news"></a> Delete News

`{{env('APP_URL')}}/api/v1/backend/news/666` [DELETE] 

Response (Status Code: 204)

#<a name="gallery"></a> Gallery
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/backend/gallery/?expand=galleryPhotos,galleryCategory  мы получим информацию об категориях и фото
Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.
## <a name="index-gallery"></a> Index Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "gallery_category_id": 1,
            "title": "Sint quis autem earum.",
            "description": "Autem placeat atque nobis quas dolorem enim et. Quos qui autem enim quia exercitationem aut. Incidunt itaque eius et molestiae. Deleniti sed quod earum omnis. Voluptas quibusdam dignissimos ut.",
            "date": "2004-11-01",           
            "memorial_images": 0,        
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59",
            "galleryPhotos": [
                {
                    "image": "http://theater.loc/storage/source/3KTpNvxLIdDRKmk1MwFRTrVXZQk0SeEdgDLCsJYL.jpeg",
                    "image_path": "source/3KTpNvxLIdDRKmk1MwFRTrVXZQk0SeEdgDLCsJYL.jpeg"
                }
            ],  
            "galleryCategory": {
                "id": 1,
                "title": "MakCategory",
                "created_at": "2019-06-08 10:30:39",
                "updated_at": "2019-06-08 10:30:39"
            }      
        }       
    ],
    "links": {
        "first": "http://theater.loc/api/v1/backend/gallery?page=1",
        "last": "http://theater.loc/api/v1/backend/gallery?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/backend/gallery",
        "per_page": 15,
        "to": 8,
        "total": 8
    }
}
```


## <a name="create-gallery"></a> Create Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery` [POST]

Fields:

```
title = String, NOT NULL
gallery_category_id = Integer, NOT NULL
description = String, NOT NULL
date = Date Format(Y-m-d), NOT NULL,
memorial_images = Integer, (1 - is memorial, 2 - not memorial)
gallery_photos = File, Array, Null (Example: gallery_photos[0] = File, gallery_photos[1] = File)
```

Response (Status Code: 201)

```json
{
    "id": 11,
    "gallery_category_id": 1,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "memorial_images": 1,   
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```


## <a name="update-gallery"></a> Update Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery/666` [PUT] 

Fields:

```
title = String, NOT NULL
gallery_category_id = Integer, NOT NULL
description = String, NOT NULL
date = Date Format(Y-m-d), NOT NULL,
memorial_images = Integer, (1 - is memorial, 2 - not memorial)
gallery_photos = File, Array, Null (Example: gallery_photos[0] = File, gallery_photos[1] = File)
delete_files = String Array, Null (Example: delete_files[0] = source/Yjnro9004KbNjfM4WpGBrFDdbyikYfrc3q4PROeN.jpeg) Указывается название файла, он приходит как атрибут image_path
```

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "memorial_images": 1,
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="show-gallery"></a> Show Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "memorial_images": 1,
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="delete-gallery"></a> Delete Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery/666` [DELETE] 

Response (Status Code: 204)


#<a name="gallery-category"></a> Gallery Category
## <a name="index-gallery-category"></a> Index GalleryCategory

`{{env('APP_URL')}}/api/v1/backend/gallery-category` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "title": "МакCategory",
            "created_at": "2019-06-08 10:30:39",
            "updated_at": "2019-06-08 10:30:39"
        }, 
    ],
    "links": {
        "first": "http://theater.loc/api/v1/backend/gallery-category?page=1",
        "last": "http://theater.loc/api/v1/backend/gallery-category?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/backend/gallery-category",
        "per_page": 15,
        "to": 8,
        "total": 8
    }
}
```

## <a name="create-gallery-category"></a> Create Gallery Category

`{{env('APP_URL')}}/api/v1/backend/gallery-category` [POST]

Fields:

```
title = String, NOT NULL
```

Response (Status Code: 201)

```json
{
    "id": 11,
    "title": "Foggy",   
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```


## <a name="update-gallery-category"></a> Update Gallery Category

`{{env('APP_URL')}}/api/v1/backend/gallery-category/666` [PUT] 

Fields:

```
title = String, NOT NULL
```

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",    
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="show-gallery-category"></a> Show Gallery Category

`{{env('APP_URL')}}/api/v1/backend/gallery-category/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",    
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="delete-gallery-category"></a> Delete Gallery

`{{env('APP_URL')}}/api/v1/backend/gallery-category/666` [DELETE] 

Response (Status Code: 204)

## Frontend Api

## <a name="play-front"></a>Play
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/play/?expand=personPhotos,plays,scenario мы получим информацию по спектаклю,
которая будет содержать дополнительно данные про фото и актеров, сценариев. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-play-front"></a> Index Play

`{{env('APP_URL')}}/api/v1/play` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 3,
            "name": "Walker, Berge and Herman",           
            "description": "Voluptates dolorem est porro itaque. Magnam qui aut repudiandae. Necessitatibus tempore ut et reprehenderit vero. Quasi ex quia et iusto facere soluta iste et.",
            "date": "1975-01-05 13:55:46",
            "status": 2,
            "image": "https://lorempixel.com/gray/320/240/cats/Faker/?18083",
            "image_path": "320/240/cats/Faker/?18083",
            "created_at": "2019-03-08 13:19:28",
            "updated_at": "2019-03-08 13:19:28"
        }       
    ],
    "links": {
        "first": "http://theater.loc/api/v1/play?page=1",
        "last": "http://theater.loc/api/v1/play?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/play",
        "per_page": 15,
        "to": 5,
        "total": 5
    }
}
```

## <a name="show-play-front"></a> Show Play

`{{env('APP_URL')}}/api/v1/play/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "name": "Don Simon",
    "genre": "деревенский сериал",
    "duration": "75",
    "description": "Alias doloribus nulla architecto et deserunt",
    "date": "2019-03-10 19:00:00",
    "status": "2",
    "image": "http://theater.loc/storage/source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "image_path": "source/2da19D1qbgsUCjnW1xdNu2XL02qaMfmrBtGpJtqm.jpeg",
    "created_at": "2019-03-12 15:50:36",
    "updated_at": "2019-03-12 16:00:19"    
}
``` 


## <a name="person-front"></a>Person
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/person/?expand=playPhotos,persons мы получим информацию об учасниках спектакля,
которая будет содержать дополнительно данные про фото и где актеры играли. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-person-front"></a> Index Person

`{{env('APP_URL')}}/api/v1/person` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "first_name": "Nona",
            "last_name": "Dicki",
            "description": "Molestiae sit quas voluptates dolorem consectetur temporibus autem. Harum dolores quo molestiae laudantium. Quos dolorem unde voluptate sapiente. Eum fuga quasi beatae vel.",
            "status": 2,
            "image": "https://lorempixel.com/gray/320/240/people/Faker/?44159",
            "image_path": "320/240/people/Faker/?44159",
            "created_at": "2019-03-08 13:19:46",
            "updated_at": "2019-03-08 13:19:46"
        }        
    ],
    "links": {
        "first": "http://theater.loc/api/v1/person?page=1",
        "last": "http://theater.loc/api/v1/person?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/person",
        "per_page": 15,
        "to": 13,
        "total": 13
    }
}
```



## <a name="news-front"></a>News
Используя параметр **expand** можно указать, какие поля должны быть включены в результат. 
Например, по адресу {{env('APP_URL')}}/api/v1/news/?expand=newsPhotos мы получим информацию об новостях,
которая будет содержать дополнительно данные про фото. Но неследует забывать, что это лишние запросы к БД,
так что использовать нужно только тогда, когда есть необходимость.

## <a name="index-news-front"></a> News

`{{env('APP_URL')}}/api/v1/news` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "title": "Sint quis autem earum.",
            "description": "Autem placeat atque nobis quas dolorem enim et. Quos qui autem enim quia exercitationem aut. Incidunt itaque eius et molestiae. Deleniti sed quod earum omnis. Voluptas quibusdam dignissimos ut.",
            "date": "2004-11-01",
            "status": 2,
            "image": "https://lorempixel.com/320/240/cats/Faker/?34848",
            "image_path": "320/240/cats/Faker/?34848",
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59",
            "newsPhotos": [
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?66906",
                    "image_path": "460/380/cats/Faker/?66906"
                },
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?52987",
                    "image_path": "460/380/cats/Faker/?52987"
                }
            ]
        },
        {
            "id": 2,
            "title": "Tempora minus est.",
            "description": "Nisi eum nihil reprehenderit et dolorem. Autem nulla iusto aspernatur aspernatur earum voluptas eum.",
            "date": "1990-11-15",
            "status": 2,
            "image": "https://lorempixel.com/320/240/cats/Faker/?51123",
            "image_path": "320/240/cats/Faker/?51123",
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59",
            "newsPhotos": [
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?52081",
                    "image_path": "460/380/cats/Faker/?52081"
                },
                {
                    "image": "https://lorempixel.com/460/380/cats/Faker/?24266",
                    "image_path": "460/380/cats/Faker/?24266"
                }
            ]
        }
    ],
    "links": {
        "first": "http://theater.loc/api/v1/news?page=1",
        "last": "http://theater.loc/api/v1/news?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/news",
        "per_page": 15,
        "to": 8,
        "total": 8
    }
}
```

## <a name="show-news-front"></a> Show News

`{{env('APP_URL')}}/api/v1/news/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "status": "2",
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```

## <a name="gallery-front"></a>Gallery

## <a name="index-gallery-front"></a>Gallery Index


`{{env('APP_URL')}}/api/v1/gallery` [GET]

Response (Status Code: 200)

```json
{
    "data": [
        {
            "id": 1,
            "title": "Sint quis autem earum.",
            "gallery_category_id": 1,
            "description": "Autem placeat atque nobis quas dolorem enim et. Quos qui autem enim quia exercitationem aut. Incidunt itaque eius et molestiae. Deleniti sed quod earum omnis. Voluptas quibusdam dignissimos ut.",
            "date": "2004-11-01",
            "memorial_images": 1,
            "image": "https://lorempixel.com/320/240/cats/Faker/?34848",
            "image_path": "320/240/cats/Faker/?34848",
            "created_at": "2019-03-08 13:19:59",
            "updated_at": "2019-03-08 13:19:59"           
        }        
    ],
    "links": {
        "first": "http://theater.loc/api/v1/gallery?page=1",
        "last": "http://theater.loc/api/v1/gallery?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/gallery",
        "per_page": 15,
        "to": 8,
        "total": 8
    }
}
```

## <a name="show-gallery-front"></a> Show Gallery

`{{env('APP_URL')}}/api/v1/gallery/666` [GET] 

Response (Status Code: 200)

```json
{
    "id": 666,
    "gallery_category_id": 1,
    "title": "Foggy",
    "description": "Est laborum vero ut occaecati. Quod voluptatibus fuga officiis neque qui recusandae laboriosam. Qui debitis eaque reprehenderit ea veniam. Quisquam at odit possimus expedita non aliquam",
    "date": "2019-05-11",
    "memorial_images": 1,
    "image": "http://theater.loc/storage/source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "image_path": "source/xyz5RWSi0wLo6PyVvTPftQs65aPL1dwRIrq47Nzm.jpeg",
    "created_at": "2019-03-19 08:52:38",
    "updated_at": "2019-03-19 08:52:38"
}
```


## <a name="search"></a> Search
Поиск осуществляется по 3м элементам: новости, пьесы и труппа. 
Для новостей и пьес идет поиск по `title`, `description`. Для персонала поиск идет по
`first_name`, `last_name`, `description`. В ответе у нас есть поле `type` которое говорит о том, какой тип 
вернул поиск, т.е. `type`: persons, news, plays. Это нам нужно для формирования ссылки на полную страницу
для конкретного элемента по его id 


`{{env('APP_URL')}}/api/v1/search/{текст поиска}` [GET] 

Response (Status Code: 200)

```json
{
    "data": [
    {
        "id": 12,
        "title": "Kaelyn Hodkiewicz",
        "description": "Sit eaque fuga sequi rerum. Animi ducimus non ea molestiae et magnam eius. Facilis sunt cupiditate sint est eos. Repellat neque vel suscipit earum expedita facere.",
        "type": "persons"
    },
    {
        "id": 3,
        "title": "Hod",
        "description": "Lorem ipsum dolor. Facilis sunt cupiditate sint est eos.",
        "type": "news"
    }
    ],
    "links": {
        "first": "http://theater.loc/api/v1/search/Hodkiewicz%20Kaelyn?page=1",
        "last": "http://theater.loc/api/v1/search/Hodkiewicz%20Kaelyn?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://theater.loc/api/v1/search/Hodkiewicz%20Kaelyn",
        "per_page": 15,
        "to": 1,
        "total": 1
    }
}
```