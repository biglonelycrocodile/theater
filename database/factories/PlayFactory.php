<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Play;
use App\PlayPhoto;

$factory->define(Play::class, function (Faker $faker) {

    $width=320;
    $height=240;
    $image = $faker->imageUrl($width, $height, 'cats', true, 'Faker', true);
    list($base_url, $path) = explode($width, $image);
    
    return [        
        'name' => $faker->company,                
        'description' => $faker->text,
        'date' => $faker->dateTime,     
        'status' => Play::STATUS_ACTIVE,
        'base_url' => $base_url,
        'path' => $width . $path
    ];
});


$factory->define(PlayPhoto::class, function (Faker $faker) {
    $width=460;
    $height=380;
    $image = $faker->imageUrl($width, $height, 'cats', true, 'Faker', true);
    list($base_url, $path) = explode($width, $image);

    $plays = Play::active()->get()->pluck('id')->toArray();

    return [       
        'play_id' => $faker->randomElement($plays),
        'base_url' => $base_url,
        'path' => $width . $path    
    ];
});




