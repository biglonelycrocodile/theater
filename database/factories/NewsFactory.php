<?php

use Faker\Generator as Faker;

use App\News;
use App\Play;
use App\NewsPhoto;

$factory->define(News::class, function (Faker $faker) {

    $width=320;
    $height=240;
    $image = $faker->imageUrl($width, $height, 'cats', true, 'Faker');
    list($base_url, $path) = explode($width, $image);
    if(random_int(0, 1) === 1) {
        $plays = Play::active()->get()->pluck('id')->toArray();
        $playId = $faker->randomElement($plays);
    } else {
        $playId = null;
    }
    return [
        'play_id' => $playId,
        'title' => $faker->sentence(3),
        'description' => $faker->text,      
        'status' => News::STATUS_ACTIVE,
        'date' =>  $faker->date,
        'base_url' => $base_url,
        'path' => $width . $path
    ];
});

$factory->define(NewsPhoto::class, function (Faker $faker) {

    $width=460;
    $height=380;
    $image = $faker->imageUrl($width, $height, 'cats', true, 'Faker');
    list($base_url, $path) = explode($width, $image);   
    $news = News::active()->get()->pluck('id')->toArray();

    return [
        'news_id' => $faker->randomElement($news),
        'base_url' => $base_url,
        'path' => $width . $path
    ];
});