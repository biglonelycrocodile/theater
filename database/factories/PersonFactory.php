<?php

use Faker\Generator as Faker;

use App\Person;
use App\PersonPhoto;

$factory->define(Person::class, function (Faker $faker) {

    $width=320;
    $height=240;
    $image = $faker->imageUrl($width, $height, 'people', true, 'Faker', true);
    list($base_url, $path) = explode($width, $image);
    $gender = ['male', 'female'];
    $genderKey = array_rand($gender);

    return [
        'first_name' => $faker->firstName($gender[$genderKey]),
        'last_name' => $faker->lastName,        
        'description' => $faker->text,      
        'status' => Person::STATUS_ACTIVE,
        'base_url' => $base_url,
        'path' => $width . $path
    ];
});

$factory->define(PersonPhoto::class, function (Faker $faker) {

    $width=460;
    $height=380;
    $image = $faker->imageUrl($width, $height, 'people', true, 'Faker', true);
    list($base_url, $path) = explode($width, $image);   
    $plays = Person::active()->get()->pluck('id')->toArray();

    return [
        'person_id' => $faker->randomElement($plays),
        'base_url' => $base_url,
        'path' => $width . $path
    ];
});