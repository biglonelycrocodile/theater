<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('galleries')->truncate();
        DB::table('gallery_photos')->truncate();
        Schema::enableForeignKeyConstraints();

        Schema::create('gallery_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->integer('gallery_category_id')->unsigned()->after('id');            
            $table->foreign('gallery_category_id')
                ->references('id')
                ->on('gallery_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        Schema::table('galleries', function (Blueprint $table) {
            $table->dropForeign(['gallery_category_id']);
            $table->dropColumn('gallery_category_id');           
        });
 
        Schema::dropIfExists('gallery_categories');
    }
}
