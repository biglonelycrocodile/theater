<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullDateDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plays', function (Blueprint $table) {           
            $table->dateTime('date')->nullable()->change();
            $table->text('description')->nullable()->change();                   
        });
    
        Schema::table('persons', function (Blueprint $table) {           
            $table->text('description')->nullable()->change();                   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plays', function (Blueprint $table) {       
            $table->text('description')->change();
            $table->dateTime('date')->change();
        });

        Schema::table('persons', function (Blueprint $table) {           
            $table->text('description')->change();                   
        });
    }
}
