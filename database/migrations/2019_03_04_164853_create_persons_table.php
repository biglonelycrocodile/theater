<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('description');
            $table->unsignedTinyInteger('status');           
            $table->string('base_url')->nullable();
            $table->string('path')->nullable();
            $table->softDeletes();
            $table->timestamps();  
        });

        Schema::create('person_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned(); 
            $table->string('base_url');
            $table->string('path');
            $table->timestamps();    
            
            $table->foreign('person_id')
                ->references('id')
                ->on('persons')
                ->onDelete('cascade');        
        });

        Schema::create('play_person', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('play_id')->unsigned(); 
            $table->integer('person_id')->unsigned(); 
            $table->string('name')->nullable();
            $table->unsignedTinyInteger('type');
            
            $table->foreign('play_id')
                ->references('id')
                ->on('plays')
                ->onDelete('cascade');        
            
            $table->foreign('person_id')
                ->references('id')
                ->on('persons')
                ->onDelete('cascade');        
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('play_person');
        Schema::dropIfExists('person_photos');
        Schema::dropIfExists('persons');
    }
}
