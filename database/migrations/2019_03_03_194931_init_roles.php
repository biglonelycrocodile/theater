<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \YaroslavMolchan\Rbac\Models\Role;
use App\User;

class InitRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRole = Role::create([
            'name' => 'Administrator',
            'slug' => User::ROLE_ADMINISTRATOR
        ]);

        $userRole = Role::create([
            'name' => 'User',
            'slug' => User::ROLE_USER
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where('slug', User::ROLE_ADMINISTRATOR)->delete();
        Role::where('slug', User::ROLE_USER)->delete();
    }
}
