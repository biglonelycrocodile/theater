<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->date('date');
            $table->unsignedTinyInteger('status');           
            $table->string('base_url')->nullable();
            $table->string('path')->nullable();
            $table->softDeletes();
            $table->timestamps();  
        });

        Schema::create('news_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned(); 
            $table->string('base_url');
            $table->string('path');
            $table->timestamps();    
            
            $table->foreign('news_id')
                ->references('id')
                ->on('news')
                ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
