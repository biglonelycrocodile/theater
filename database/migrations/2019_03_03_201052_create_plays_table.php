<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('genre');
            $table->unsignedSmallInteger('duration');
            $table->text('description');
            $table->dateTime('date');            
            $table->unsignedTinyInteger('status');           
            $table->string('base_url')->nullable();
            $table->string('path')->nullable();
            $table->softDeletes();
            $table->timestamps();            
        });

        Schema::create('play_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('play_id')->unsigned(); 
            $table->string('base_url');
            $table->string('path');
            $table->timestamps();    
            
            $table->foreign('play_id')
                ->references('id')
                ->on('plays')
                ->onDelete('cascade');        
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {      
        Schema::dropIfExists('play_photos');       
        Schema::dropIfExists('plays');
    }
}
