<?php

use Illuminate\Database\Seeder;
use App\User;
use YaroslavMolchan\Rbac\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /** Create admin */
    	$admin =  User::create([
            'name' => 'webmaster',
            'email' => 'webmaster@example.com',
            'password' => 'webmaster',
            'status' => User::STATUS_ACTIVE,
            'api_token' => str_random(60)
        ]);
        $adminRole = Role::where('slug',User::ROLE_ADMINISTRATOR)->get();
		$admin->attachRole($adminRole);
      
    }
}
