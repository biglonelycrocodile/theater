<?php

use Illuminate\Database\Seeder;
use App\Play;
use App\Person;

class PersonTableSeeder extends Seeder
{
    const MAX_PERSON = 12;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Person::class, self::MAX_PERSON)->create();
        factory(App\PersonPhoto::class, 30)->create();

        $plays = Play::active()->get()->pluck('id')->toArray();
        $persons = Person::active()->get()->pluck('id')->toArray();

        
        $data = [];
        $percentPersons = round(35 * self::MAX_PERSON / 100);
        $percentTypes = round(15 * self::MAX_PERSON / 100);

        foreach($plays as $playId) {
            foreach ($persons as  $personId) {
                $randPerson = random_int(1, self::MAX_PERSON);
                if($randPerson > $percentPersons) {
                    $faker = \Faker\Factory::create();                    
                    $randType = random_int(1, self::MAX_PERSON);
                    $data[] = [
                        'play_id' => $playId,
                        'person_id' => $personId,
                        'name' => $faker->name,
                        'type' => ($randType > $percentTypes) ? Play::PERSON_TYPE_ACTOR : Play::PERSON_TYPE_DIRECTOR
                    ];
                }               
            }   
        }
        
        DB::table('play_person')->insert($data);                
    }
}
