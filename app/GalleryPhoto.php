<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryPhoto extends Model
{
    protected $fillable = [
        'gallery_id', 'base_url', 'path'
    ];

    public function galleries()
    {
        return $this->belongsTo('App\Gallery');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }
}