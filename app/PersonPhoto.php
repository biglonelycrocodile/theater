<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonPhoto extends Model
{
    protected $fillable = [
        'person_id', 'base_url', 'path'
    ];

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }
}