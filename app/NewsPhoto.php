<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsPhoto extends Model
{
    protected $fillable = [
        'news_id', 'base_url', 'path'
    ];

    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }
}