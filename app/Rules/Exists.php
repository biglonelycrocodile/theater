<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DB;

class Exists implements Rule
{
    protected $query;
    protected $column;
    protected $errors = [];

    public function __construct($table, $column = null, $conditions = [])
    {
        $this->query = DB::table($table);
        $this->column = $column;
        foreach($conditions as $key => $condition) {
            $this->query->$key($condition);
        }        
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {      
        return $this->getResult($attribute, $value);
    }

    protected function getResult($attribute, $value)
    {
        $column = $this->column ?? $attribute;        
        if(is_array($value)) {
            $result = $this->query->whereIn($column, $value)->pluck($column)->toArray();
            return $this->multipleValue($result, $value, $attribute);
        } else {           
            $exists = $this->query->where($column, $value)->exists();                      
            return $this->singleValue($exists, $attribute);
        }                    
    }

    protected function singleValue($exists, $attribute) 
    {
       if($exists === false) {
           $this->errors[] = "The selected {$attribute} is invalid.";
           return false;
       }
       return true;
    }

    protected function multipleValue($result, $value, $attribute) 
    {
        if(($incorrectValues = array_diff($value, $result)) !== []){ 
           array_map(function($index) use($attribute){
               $this->errors[] = "The selected {$attribute}.$index is invalid.";
           }, array_keys($incorrectValues));           
           return false;
        } 
        return true;
    }
    

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {   //TODO во первых тут нужно вынести в __trans 
        //во вторых множество значений должно иметь множество выводов об ошибках       
        return $this->errors;
    }

   



 
    
    
}