<?php

namespace App\Components;

use Illuminate\Http\Request;
use ReflectionClass;
use Carbon\Carbon;
use File;
use  Illuminate\Database\Eloquent\Collection;

class FileKit
{
    public $field;
    public $multiple = false;
    public $baseUrl = 'base_url';
    public $path = 'path';
    public $uploadRelation;
    

    protected $request;
    protected $model;
    protected $sourcePath;
    protected $publicUrl;

    /**
     * @param Request request
     * @param Illuminate\Database\Eloquent\Model $model
     * @param array $params
     */
    public function __construct(Request $request, $model, $params = [])
    {
        $this->request = $request;
        $this->model = $model;
        $this->initProperties($params);  
        $this->setFilePaths();      
    }
    /**
     * Save one file and set model's fields. 
     */
    public function saveFile()
    {  
        if($this->request->hasFile($this->field)) {
            $file = $this->request->file($this->field, null);       
            if($file !== null) {
               $this->model->{$this->baseUrl} = $this->publicUrl;
               $this->model->{$this->path} = $file->store($this->sourcePath);
            }
        }
        $this->deleteFiles();
    }

    /**
     * Save multiple files and set model's fields.
     */
    public function saveFiles()
    {//ToDo тут проблема иногда бывает hasMany и hasOne и дублируюется записи для последнего
        $hasMany = $this->model->{$this->uploadRelation}();          
        $relationModel = $hasMany->getRelated();
        $fileData = [];
        
        if($this->request->hasFile($this->field)) {        
            $files = $this->request->file($this->field, []);
            if(is_object($files)) {		 	
                $files = [$files];
            }           
            $time = Carbon::now();
            foreach ($files as $index => $file) {						
                $fileData[$index][$this->baseUrl] = $this->publicUrl;
                $fileData[$index][$this->path] = $file->store($this->sourcePath);
                $fileData[$index][$hasMany->getForeignKeyName()] = $this->model->id;
                if($relationModel->timestamps === true) {
                    $fileData[$index]['created_at'] = $time;
                    $fileData[$index]['updated_at'] = $time;
                }
            }    
        }    
        $relationModel::insert($fileData);
        $this->deleteFiles();
    }

    protected function deleteFiles()
    {
        $deleteFiles = $this->request->input('delete_files',[]);
        if(is_string($deleteFiles)) {
            $deleteFiles = [$deleteFiles];
        }
        if($this->multiple === false) {
            $this->deleteOneFile($deleteFiles);
        } else {
            $this->deleteMultipleFiles($deleteFiles);
        }
    }

    protected function deleteOneFile($files)
    {
        foreach ($files as $file) {
            if($this->model->{$this->path} == $file) {
                $this->model->{$this->path} = null;
                $this->model->{$this->baseUrl} = null;
                $this->removeFile($file);
                break;
            }
        }        
    }

    protected function deleteMultipleFiles($files)
    {
        $relatedModels =  $this->model->{$this->uploadRelation};
        if(!$relatedModels instanceof Collection) {
            $relatedModels = (new Collection())->add($relatedModels);           
        }

        foreach ($files as $file) {
            foreach($relatedModels as $model) {
                if($model->{$this->path} == $file) {
                    $model->delete();
                    $this->removeFile($file);
                }
            }
        }
        $this->model->load($this->uploadRelation); 
    }

    protected function removeFile($path)
	{
		$rootPath = config('filesystems.disks.public.root');
		$fullPath = $rootPath . '/' . $path;
		File::delete($fullPath);		
	}


    protected function initProperties($params)
    {
        $reflection = new ReflectionClass($this);
        foreach ($reflection->getProperties() as $property) {
            if($property->isPublic() && isset($params[$property->name])) {
                $property->setValue($this, $params[$property->name]);
            }            
        }
    }

    /**
     * Setting coinfig properties.
     */
    protected function setFilePaths()
    {
        $this->sourcePath = 'source';
        $this->publicUrl = config('filesystems.disks.public.url') . '/';
    }

}