<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;

    const IS_MEMORIAL_IMAGES  = 1;
    const NOT_MEMORIAL_IMAGES = 0;

    protected $fillable = [
        'title', 'description', 'date', 'base_url', 'path', 'memorial_images', 
        'gallery_category_id'
    ];

    protected $attributes = [
        'memorial_images' => self::NOT_MEMORIAL_IMAGES       
    ];

    protected $dates = ['deleted_at'];

    public $fileUpload  = [];      
    public $fileUploads = [
        [
            'field' => 'gallery_photos',
            'uploadRelation' => 'galleryPhotos',
            'multiple' => true
        ]        
    ];

  

    public static function getMemorialImages()
    {
        return [
            self::IS_MEMORIAL_IMAGES  => 'Memorial Image',
            self::NOT_MEMORIAL_IMAGES => 'Not Memorial Image',
        ];
    }

    public function galleryPhotos()
    {
        return $this->hasMany('App\GalleryPhoto', 'gallery_id');
    }

    public function galleryCategory()
    {
        return $this->belongsTo('App\GalleryCategory', 'gallery_category_id');
    }
}
