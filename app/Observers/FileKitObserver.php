<?php

namespace App\Observers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Components\FileKit;

class FileKitObserver
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function saving(Model $model)
    {       
        $fileKit = new FileKit($this->request, $model, $model->fileUpload);
        $fileKit->saveFile();              
    }

    public function saved(Model $model)
    {
        array_map(function ($element) use($model) {
            $fileKit = new FileKit($this->request, $model, $element);
            $fileKit->saveFiles();
        },$model->fileUploads);        
    } 
}