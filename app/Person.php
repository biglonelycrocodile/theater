<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;
    
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 2;

    const IS_MEMORIAL_IMAGES  = 1;
    const NOT_MEMORIAL_IMAGES = 0;

    protected $table = 'persons';

    protected $attributes = [
        'memorial_images' => self::NOT_MEMORIAL_IMAGES       
    ];

    protected $fillable = [
        'first_name', 'last_name', 'description', 'status', 'base_url', 'path', 'memorial_images'
    ];

    protected $dates = ['deleted_at'];

    public $fileUpload = [                   
        'field' => 'person_photo'         
    ];

    public $fileUploads = [
        [
            'field' => 'person_photos',
            'uploadRelation' => 'personPhotos',
            'multiple' => true
        ]        
    ];

    public function scopeActive($query)
    {
        return $query->where('persons.status', self::STATUS_ACTIVE);
    }

    public static function getMemorialImages()
    {
        return [
            self::IS_MEMORIAL_IMAGES  => 'Memorial Image',
            self::NOT_MEMORIAL_IMAGES => 'Not Memorial Image',
        ];
    }

    public static function getStatuses()
    {
        $statuses =  [
            self::STATUS_NOT_ACTIVE => __('messages.not_active'),
            self::STATUS_ACTIVE => __('messages.active'),           
        ];        
        return $statuses;
    }

    public static function getStatus($status)
    {
        $statuses = self::getStatuses();
        return ($statuses[$status]) ?? __('messages.no_set');
    }

    public function personPhotos()
    {
        return $this->hasMany('App\PersonPhoto', 'person_id');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }

    public function plays()
    {
        return $this->belongsToMany('App\Play', 'play_person', 'person_id', 'play_id')
                    ->withPivot('name', 'type', 'id');
    }

}
