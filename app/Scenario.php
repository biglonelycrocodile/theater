<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scenario extends Model
{

    protected $fillable = [
        'play_id', 'base_url', 'path'
    ];


    public function play()
    {
        return $this->belongsTo('App\Play');
    }

    public function getScenario()
    {
        return $this->base_url . $this->path;
    }

}