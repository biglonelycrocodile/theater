<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use YaroslavMolchan\Rbac\Traits\Rbac;

class User extends Authenticatable
{
    use Rbac;
    use Notifiable;

    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 2;

    const ROLE_USER = 'user';
    const ROLE_ADMINISTRATOR = 'administrator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->where('users.status', self::STATUS_ACTIVE);
    }

    public function scopeAdmin($query)
    {        
        return $query->rightJoin('roles', function($join){
                            $join->where('roles.slug', '=', self::ROLE_ADMINISTRATOR);
                     })
                     ->rightJoin('role_user', function($join){                     
                         $join->on('role_user.user_id', '=', 'users.id');
                         $join->on('role_user.role_id', '=', 'roles.id');
                     });
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public  function validatePassword($password)
    {
        return \Hash::check($password, $this->password); 
    }

    /** Это для создание юзера через FB, добавить миграцию 
     * add field facebok_id to table `users`
     */
    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();
        if(is_null($check)){
            return static::create($input);
        }
        return $check;
    }

    public static function findAdminByEmail($email)
    {
        return self::where('users.email', $email)->active()->admin()->first();
    }
}
