<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Play extends Model
{
    use SoftDeletes;
    
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_FINISHED = 3;

    const PERSON_TYPE_DIRECTOR = 1;
    const PERSON_TYPE_ACTOR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'date', 
        'status', 'base_url', 'path'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public $fileUpload = [                   
        'field' => 'play_photo'         
    ];

    public $fileUploads = [
        [
            'field' => 'play_photos',
            'uploadRelation' => 'playPhotos',
            'multiple' => true
        ],        
        [
            'field' => 'scenario',
            'uploadRelation' => 'scenario',
            'multiple' => true
        ]        
    ];
    


    public function scopeActive($query)
    {
        return $query->where('plays.status', self::STATUS_ACTIVE);
    }

    public static function getStatuses()
    {
        $statuses =  [
            self::STATUS_NOT_ACTIVE => __('messages.not_active'),
            self::STATUS_ACTIVE => __('messages.active'),           
            self::STATUS_FINISHED => __('messages.finished'),           
        ];        
        return $statuses;
    }

    public static function getStatus($status)
    {
        $statuses = self::getStatuses();
        return ($statuses[$status]) ?? __('messages.no_set');
    }

    public static function getPersonTypes()
    {
        $types =  [
            self::PERSON_TYPE_DIRECTOR => __('messages.director'),
            self::PERSON_TYPE_ACTOR => __('messages.actor'),           
        ];        
        return $types;
    }

    public static function getPersonType($type)
    {
        $statuses = self::getPersonTypes();
        return ($statuses[$type]) ?? __('messages.no_set');
    }
  

    public function playPhotos()
    {
        return $this->hasMany('App\PlayPhoto', 'play_id');
    }

    public function scenario()
    {
        return $this->hasOne('App\Scenario', 'play_id');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }

    public function persons()
    {
        return $this->belongsToMany('App\Person', 'play_person', 'play_id', 'person_id')
                    ->withPivot('name', 'type','id');
    }

}
