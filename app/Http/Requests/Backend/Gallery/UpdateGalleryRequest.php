<?php

namespace App\Http\Requests\Backend\Gallery;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Gallery;

class UpdateGalleryRequest extends FormRequest
{

    public function authorize()
    {    
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [           
            'title' => 'nullable|max:255',
            'gallery_category_id' => [
            	'required',
            	'integer',
            	Rule::exists('gallery_categories', 'id')->where(function ($query){ 	
            		$query->whereNull('deleted_at');    
            	})
            ],
            'description' => 'nullable|string',
            'date' => 'nullable|date_format:Y-m-d',       
            'memorial_images' => [
            	'nullable',
            	 Rule::in(array_keys(Gallery::getMemorialImages())),
            ],                     
            'galler_photos.*' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',                  
        ];
    }


}