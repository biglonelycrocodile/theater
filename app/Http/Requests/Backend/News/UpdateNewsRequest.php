<?php

namespace App\Http\Requests\Backend\News;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\News;
use App\Play;

class UpdateNewsRequest extends FormRequest
{

    public function authorize()
    {    
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [           
            'title' => 'required|max:255',
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d',
            'play_id' => [
            	'nullable',
            	'integer',
            	Rule::exists('plays', 'id')->where(function ($query){ 	
            		$query->where('status', Play::STATUS_ACTIVE);
            	})
            ],
            'status' => [
            	'required',
            	 Rule::in(array_keys(News::getStatuses())),
            ],            
            'news_photo' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
            'news_photos.*' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
            'delete_files.*' => 'nullable|string'
        ];
    }


}