<?php

namespace App\Http\Requests\Backend\Play;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Rule;
use App\Play;
use App\Person;

class StorePlayRequest extends FormRequest
{

    public function authorize()
    {    
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [           
            'name' => 'required|max:255',            
            'date' => 'required|date_format:Y-m-d H:i:s',            
            'description' => 'required|string',
            'status' => [
            	'required',
            	 Rule::in(array_keys(Play::getStatuses())),
            ],
            'persons.*' => [
            	'nullable',
            	'integer',
            	Rule::exists('persons', 'id')->where(function ($query){ 	
            		$query->where('status', Person::STATUS_ACTIVE);
            	})
            ],
            'scenario' => 'nullable|max:5048|mimes:doc,docx,pdf',            
            'play_photo' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
            'play_photos.*' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
        ];
    }


}