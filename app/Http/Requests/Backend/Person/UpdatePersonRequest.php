<?php

namespace App\Http\Requests\Backend\Person;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Person;

class UpdatePersonRequest extends FormRequest
{

    public function authorize()
    {    
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [           
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'description' => 'required|string',
            'memorial_images' => [
            	'nullable',
            	Rule::in(array_keys(Person::getMemorialImages())),
            ],  
            'status' => [
            	'required',
            	 Rule::in(array_keys(Person::getStatuses())),
            ],            
            'person_photo' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
            'person_photos.*' => 'nullable|max:2048|mimes:jpeg,jpg,bmp,png',            
            'delete_files.*' => 'nullable|string'
        ];
    }


}