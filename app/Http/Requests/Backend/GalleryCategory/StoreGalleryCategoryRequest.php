<?php

namespace App\Http\Requests\Backend\GalleryCategory;

use Illuminate\Foundation\Http\FormRequest;

class StoreGalleryCategoryRequest extends FormRequest
{

    public function authorize()
    {    
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [           
            'title' => 'nullable|max:255'                            
        ];
    }


}