<?php

namespace App\Http\Resources\Backend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Backend\PlayPhotoResource;
use App\Http\Resources\Backend\PlayPersonResource;

class ScenarioResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [    		
            'scenario' => $this->getScenario(),
            'scenario_path' => $this->path,         
        ];
        
        return array_merge($data, $this->extraFields);
    }
}
