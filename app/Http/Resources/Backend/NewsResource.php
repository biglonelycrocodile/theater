<?php

namespace App\Http\Resources\Backend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Backend\NewsPhotoResource;

class NewsResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'title' => $this->title,
            'description' => $this->description,
            'play_id' => $this->play_id,
            'date' => $this->date,
            'status' => $this->status,
            'image' => $this->getImage(),
            'image_path' => $this->path,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];

        return array_merge($data, $this->extraFields);
    }

    public function extraFields()
    {
        return ['newsPhotos', 'play'];
    }

    public function newsPhotos()
    {
        return NewsPhotoResource::collection($this->newsPhotos);
    }     

    public function play()
    {
        return new PlayResource($this->play);
    }
}
