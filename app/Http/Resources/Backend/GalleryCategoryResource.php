<?php

namespace App\Http\Resources\Backend;
use App\Http\Resources\GeneralResource;

class GalleryCategoryResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'title' => $this->title,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];
        
        return $data;
    }

   
  
}
