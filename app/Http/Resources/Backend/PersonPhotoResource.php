<?php

namespace App\Http\Resources\Backend;
use App\Http\Resources\GeneralResource;

class PersonPhotoResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'image' => $this->getImage(),  
            'image_path' => $this->path                      
    	];

        return array_merge($data, $this->extraFields);
    }
}
