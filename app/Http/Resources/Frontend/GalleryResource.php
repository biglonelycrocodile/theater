<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Frontend\GalleryPhotoResource;
use App\Http\Resources\Frontend\GalleryCategoryResource;

class GalleryResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'title' => $this->title,
            'description' => $this->description,
            'date' => $this->date,
            'memorial_images' => $this->memorial_images,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];

        return array_merge($data, $this->extraFields);
    }

    public function extraFields()
    {
        return ['galleryPhotos','galleryCategory'];
    }

    public function galleryPhotos()
    {
        return GalleryPhotoResource::collection($this->galleryPhotos);
    }

    public function galleryCategory()
    {
        return new GalleryCategoryResource($this->galleryCategory);
    }
  
}
