<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;

class SearchResource extends GeneralResource
{
    public function toArray($request)
    {       
    	$data =  [    		
            'id' => $this->id,
            'title' => $this->title,         
            'description' => $this->description,         
            'type' => $this->type,         
        ];
        
        return array_merge($data, $this->extraFields);
    }
}
