<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Frontend\PersonPhotoResource;
use App\Http\Resources\Frontend\PlayPersonResource;

class PersonResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'description' => $this->description,
            'status' => $this->status,
            'image' => $this->getImage(),
            'image_path' => $this->path,
            'memorial_images' => $this->memorial_images,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];
       
        return array_merge($data, $this->extraFields);
    }

    public function extraFields()
    {
        return ['personPhotos', 'plays'];
    }

    public function personPhotos()
    {
        return PersonPhotoResource::collection($this->personPhotos);
    }
    public function plays()
    {
        return PlayPersonResource::collection($this->plays);
    }

 
}
