<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Frontend\GalleryResource;

class GalleryCategoryResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'title' => $this->title,           
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];

        return array_merge($data, $this->extraFields);
    }

    public function extraFields()
    {
        return ['galleries'];
    }

    public function galleries()
    {
        return GalleryResource::collection($this->galleries);
    }
  
}
