<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;
use App\Http\Resources\Frontend\PlayPhotoResource;
use App\Http\Resources\Frontend\PlayPersonResource;
use App\Http\Resources\Frontend\ScenarioResource;

class PlayResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->id,    		
            'name' => $this->name,
            'genre' => $this->genre,
            'duration' => $this->duration,
            'description' => $this->description,
            'date' => $this->date,           
            'status' => $this->status,
            'image' => $this->getImage(),
            'image_path' => $this->path,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),           
    	];

        return array_merge($data, $this->extraFields);
    }

    public function extraFields()
    {
        return ['playPhotos', 'persons', 'scenario'];
    }

    public function playPhotos()
    {
        return PlayPhotoResource::collection($this->playPhotos);
    }

    public function persons()
    {
        return PlayPersonResource::collection($this->persons);
    }

    public function scenario()
    {
        return new ScenarioResource($this->scenario);
    }
}
