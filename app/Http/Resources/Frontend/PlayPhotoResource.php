<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;

class PlayPhotoResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [    		
            'image' => $this->getImage(),  
            'image_path' => $this->path                   
    	];

        return array_merge($data, $this->extraFields);
    }
}
