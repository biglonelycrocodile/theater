<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;

class PlayPersonResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [
    		'id' => $this->pivot->id,
            'play_id' => $this->pivot->play_id,                     
            'person_id' => $this->pivot->person_id,                     
            'name' => $this->pivot->name,                     
            'type' => $this->pivot->type,                     
    	];

        return array_merge($data, $this->extraFields);
    }
}
