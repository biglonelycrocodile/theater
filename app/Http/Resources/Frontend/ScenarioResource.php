<?php

namespace App\Http\Resources\Frontend;
use App\Http\Resources\GeneralResource;

class ScenarioResource extends GeneralResource
{
    public function toArray($request)
    {
    	$data =  [    		
            'scenario' => $this->getScenario(),
            'scenario_path' => $this->path,         
        ];
        
        return array_merge($data, $this->extraFields);
    }
}
