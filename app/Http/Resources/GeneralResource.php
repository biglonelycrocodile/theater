<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Input;

class GeneralResource extends Resource
{
	protected $extraFields = [];

	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->init();		
	}

	protected function init()
	{		
		foreach ($this->getExtraFields() as $field) {
			if(method_exists($this, $field)){
				$this->extraFields[$field] = $this->$field();
			}
		}		
	}

	public function extraFields()
	{
		return [];
	}

	protected function getExtraFields()
	{	
		$expandFields = explode(',', Input::get('expand'));
		return array_intersect($expandFields, $this->extraFields());		
	}
}