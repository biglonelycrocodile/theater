<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Services\Backend\PlayService;
use App\Http\Controllers\Controller;


class PlayController extends Controller
{
    
    protected $plays;

    public function __construct(PlayService $service)
    {
        $this->plays = $service;
    }
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->plays->getFiltredPlays($request);
    }


    public function show($id)
    {
        $play = $this->plays->getPlay($id);
        return response()->json($play);
    }

}
