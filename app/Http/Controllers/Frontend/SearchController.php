<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Frontend\SearchService;

class SearchController extends Controller
{
   
    public $search;

    public function __construct(SearchService $service)
    {
        $this->search = $service;
    }

    public function index(Request $request)
    {
        return $this->search->getSearchModels($request);
    }
}