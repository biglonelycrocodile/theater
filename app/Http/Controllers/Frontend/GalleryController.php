<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Services\Frontend\GalleryService;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    protected $gallery;

    public function __construct(GalleryService $service)
    {
        $this->gallery = $service;
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->gallery->getFiltredGallery($request);
    }   

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = $this->gallery->getGallery($id);
        return response()->json($gallery);
    }   
}
