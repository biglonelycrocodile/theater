<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Services\Frontend\NewsService;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    protected $news;

    public function __construct(NewsService $service)
    {
        $this->news = $service;
    }

    public function afisha(Request $request)
    {  
        return $this->news->getFiltredAfishaNews($request);
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->news->getFiltredNews($request);
    }   

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->news->getNews($id);
        return response()->json($news);
    }   
}
