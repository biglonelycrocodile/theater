<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Frontend\PersonService;

class PersonController extends Controller
{

    protected $persons;

    public function __construct(PersonService $service)
    {
        $this->persons = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        return $this->persons->getFiltredPersons($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = $this->persons->getPerson($id);
        return response()->json($person);
    }

 
   
}
