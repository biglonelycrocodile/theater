<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\GalleryCategory\StoreGalleryCategoryRequest;
use App\Http\Requests\Backend\GalleryCategory\UpdateGalleryCategoryRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\Backend\GalleryCategoryService;

class GalleryCategoryController extends Controller
{
    protected $galleryCategory;

    public function __construct(GalleryCategoryService $service)
    {
        $this->galleryCategory = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->galleryCategory->getFiltredGalleryCategory($request);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGalleryCategoryRequest $request)
    {
        $gallery = $this->galleryCategory->createGalleryCategory($request); 
        return response()->json($gallery, 201);  
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = $this->galleryCategory->getGalleryCategory($id);
        return response()->json($gallery);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGalleryCategoryRequest $request, $id)
    {
        try {
            $gallery = $this->galleryCategory->updateGalleryCategory($request, $id); 
            return response()->json($gallery, 200);
        } catch(ModelNotFoundException $e) {           
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->galleryCategory->deleteGalleryCategory($id); 
            return response()->make('', 204);
        } catch(ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } 
    }
}
