<?php

namespace App\Http\Controllers\Backend;

use App\News;
use Illuminate\Http\Request;
use App\Services\Backend\NewsService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\News\StoreNewsRequest;
use App\Http\Requests\Backend\News\UpdateNewsRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NewsController extends Controller
{
    protected $news;

    public function __construct(NewsService $service)
    {
        $this->news = $service;
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->news->getFiltredNews($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNewsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request)
    {  
        $news = $this->news->createNews($request); 
        return response()->json($news, 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->news->getNews($id);
        return response()->json($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewsRequest  $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewsRequest $request, $id)
    {
        try {
            $news = $this->news->updateNews($request, $id); 
            return response()->json($news, 200);
        } catch(ModelNotFoundException $e) {           
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->news->deleteNews($id); 
            return response()->make('', 204);
        } catch(ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } 
    }
}
