<?php

namespace App\Http\Controllers\Backend;

use App\Play;
use Illuminate\Http\Request;
use App\Services\Backend\PlayService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Play\StorePlayRequest;
use App\Http\Requests\Backend\Play\UpdatePlayRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlayController extends Controller
{
    
    protected $plays;

    public function __construct(PlayService $service)
    {
        $this->plays = $service;
    }
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->plays->getFiltredPlays($request);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlayRequest $request)
    {
        $play = $this->plays->createPlay($request); 
        return response()->json($play, 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $play = $this->plays->getPlay($id);
        return response()->json($play);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlayRequest $request, $id)
    {   
        try {
            $play = $this->plays->updatePlay($request, $id); 
            return response()->json($play, 200);
        } catch(ModelNotFoundException $e) {           
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $play = $this->plays->deletePlay($id); 
            return response()->make('', 204);
        } catch(ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } 
    }

    public function test()
    {
        dd(1);
    }
}
