<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Gallery\StoreGalleryRequest;
use App\Http\Requests\Backend\Gallery\UpdateGalleryRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\Backend\GalleryService;

class GalleryController extends Controller
{
    protected $gallery;

    public function __construct(GalleryService $service)
    {
        $this->gallery = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->gallery->getFiltredGallery($request);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGalleryRequest $request)
    {
        $gallery = $this->gallery->createGallery($request); 
        return response()->json($gallery, 201);  
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = $this->gallery->getGallery($id);
        return response()->json($gallery);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGalleryRequest $request, $id)
    {
        try {
            $gallery = $this->gallery->updateGallery($request, $id); 
            return response()->json($gallery, 200);
        } catch(ModelNotFoundException $e) {           
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->gallery->deleteGallery($id); 
            return response()->make('', 204);
        } catch(ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } 
    }
}
