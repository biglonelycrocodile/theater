<?php

namespace App\Http\Controllers\Backend;

use App\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Backend\PersonService;
use App\Http\Requests\Backend\Person\StorePersonRequest;
use App\Http\Requests\Backend\Person\UpdatePersonRequest;

class PersonController extends Controller
{

    protected $persons;

    public function __construct(PersonService $service)
    {
        $this->persons = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        return $this->persons->getFiltredPersons($request);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePersonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonRequest $request)
    {
        $person = $this->persons->createPerson($request); 
        return response()->json($person, 201);    
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = $this->persons->getPerson($id);
        return response()->json($person);
    }

 
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePersonRequest  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePersonRequest $request, $id)
    {
        try {
            $person = $this->persons->updatePerson($request, $id); 
            return response()->json($person, 200);
        } catch(ModelNotFoundException $e) {           
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $person = $this->persons->deletePerson($id); 
            return response()->make('', 204);
        } catch(ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } 
    }
}
