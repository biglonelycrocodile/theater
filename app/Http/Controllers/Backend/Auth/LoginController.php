<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Auth\LoginController as BasicLogin;
use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends BasicLogin
{
    public function login(Request $request)
    {
        $this->validateLogin($request);
		
		$user = User::findAdminByEmail($request->input('email'));			
		
		if ($user === null || !$user->validatePassword($request->input('password'))) {
			$this->sendFailedLoginResponse($request);
		}
		Auth::login($user, $request->filled('remember'));
		return response()->json(['token' => $user->api_token]);
	}
}