<?php

namespace App\SearchModels;

use Illuminate\Database\Eloquent\Builder;
use ReflectionClass;

trait SearchTrait
{
	public static function applyDecoratorsFromFields($fields, Builder $query)
    {   
        foreach ($fields as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);           
            if (static::isValidDecorator($decorator) && !is_null($value)) {
                $query = $decorator::apply($query, $value);
            }          
        }
        return $query;
    }

    public static function createFilterDecorator($name)
    {  
        $reflector = new ReflectionClass(get_called_class()); 
        return $reflector->getNamespaceName() . '\\Filters\\' . studly_case($name);
    }


    public static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}