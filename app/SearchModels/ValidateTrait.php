<?php

namespace App\SearchModels;

use Illuminate\Support\Facades\Validator;

trait ValidateTrait
{
	public function validate($fields, $rules) :bool
	{
		session()->forget('errors');
		$validator = Validator::make($fields, $rules);
		if ($validator->fails()) {
			session()->flash('errors', $validator->messages()->getMessages());	
			return false;
		}
		return true;
	}
}