<?php

namespace App\SearchModels\GalleryCategory;

use App\GalleryCategory;
use App\SearchModels\{
	SearchTrait,
	SortableTrait,
	ValidateTrait
};

class GalleryCategorySearch extends GalleryCategory
{
  use SearchTrait;
  use SortableTrait;
  use ValidateTrait;
    
  protected $table = 'gallery_categories';

  public $sortable = ['id', 'title','created_at'];

  protected $rules = [
		'id' => 'integer|nullable',
		'title' => 'string|nullable',
		'created_at' => 'date|nullable'	
  ];
    
  public function search($request)
	{	
		$querySearch = (new \ReflectionClass($this))->getShortName();	
		$fields = $request->query->get($querySearch, $default = []);
		$query = GalleryCategory::sortable();			

		if(!$this->validate($fields, $this->rules)){
			return $query;
		}
      	
		return self::applyDecoratorsFromFields($fields, $query);
	}
}
