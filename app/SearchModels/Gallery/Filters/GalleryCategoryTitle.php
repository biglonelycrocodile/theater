<?php
namespace App\SearchModels\Gallery\Filters;

use Illuminate\Database\Eloquent\Builder;
use App\SearchModels\FilterInterface;

class GalleryCategoryTitle implements FilterInterface
{
	 /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereHas('galleryCategory', function($q) use ($value){
            $q->where('gallery_categories.title', 'LIKE', '%' . $value . '%');
        });   
    }
}