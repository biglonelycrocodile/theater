<?php

namespace App\SearchModels\Gallery;

use App\Gallery;
use App\SearchModels\{
	SearchTrait,
	SortableTrait,
	ValidateTrait
};

class GallerySearch extends Gallery
{
  use SearchTrait;
  use SortableTrait;
  use ValidateTrait;
    
  protected $table = 'galleries';

  public $sortable = ['id', 'title', 'description', 'date', 'memorial_images','created_at'];

  protected $rules = [
		'id' => 'integer|nullable',
		'title' => 'string|nullable',
		'date' => 'date|nullable',
		'description' => 'string|nullable',		
		'memorial_images' => 'integer|nullable',
		'created_at' => 'date|nullable'	
  ];
    
  public function search($request)
	{	
		$querySearch = (new \ReflectionClass($this))->getShortName();	
		$fields = $request->query->get($querySearch, $default = []);
		$query = Gallery::sortable();			
		
		if(!$this->validate($fields, $this->rules)){
			return $query;
		}
      	
		return self::applyDecoratorsFromFields($fields, $query);
	}
}
