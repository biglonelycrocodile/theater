<?php
namespace App\SearchModels\News\Filters;

use Illuminate\Database\Eloquent\Builder;
use App\SearchModels\FilterInterface;

class Status implements FilterInterface
{
	 /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('news.status', $value);
    }
}
