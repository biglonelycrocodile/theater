<?php

namespace App\SearchModels\News;

use App\News;
use App\SearchModels\{
	SearchTrait,
	SortableTrait,
	ValidateTrait
};

class AfishaNewsSearch extends News
{
  use SearchTrait;
  use SortableTrait;
  use ValidateTrait;
    
  protected $table = 'news';

  public $sortable = ['id', 'title', 'description', 'date', 'status', 'created_at'];

  protected $rules = [
		'id' => 'integer|nullable',
		'title' => 'string|nullable',
		'date' => 'date|nullable',
		'description' => 'string|nullable',		
		'status' => 'integer|nullable',
		'created_at' => 'date|nullable'	
  ];
    
  public function search($request)
	{	
		$querySearch = (new \ReflectionClass($this))->getShortName();	
		$fields = $request->query->get($querySearch, $default = []);
		$query = News::sortable()->with('newsPhotos')->whereNotNull('play_id');			

		if(!$this->validate($fields, $this->rules)){
			return $query;
		}
      	
		return self::applyDecoratorsFromFields($fields, $query);
	}
}
