<?php

namespace App\SearchModels\Person;

use App\Person;
use App\SearchModels\{
	SearchTrait,
	SortableTrait,
	ValidateTrait
};

class PersonSearch extends Person
{
  use SearchTrait;
	use SortableTrait;
  use ValidateTrait;
    
  protected $table = 'persons';

  public $sortable = ['id', 'first_name', 'last_name', 'description', 'status', 'created_at'];

  protected $rules = [
		'id' => 'integer|nullable',
		'first_name' => 'string|nullable',
		'last_name' => 'string|nullable',
		'description' => 'string|nullable',		
		'status' => 'integer|nullable',
		'created_at' => 'date|nullable',
  ];
    
  public function search($request)
	{	
		$querySearch = (new \ReflectionClass($this))->getShortName();	
		$fields = $request->query->get($querySearch, $default = []);
		$query = Person::sortable()->with('personPhotos')->with('plays');			

		if(!$this->validate($fields, $this->rules)){
			return $query;
		}
      	
		return self::applyDecoratorsFromFields($fields, $query);
	}
}
