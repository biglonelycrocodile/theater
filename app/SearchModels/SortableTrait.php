<?php

namespace App\SearchModels;
use Kyslik\ColumnSortable\Sortable;
use Kyslik\ColumnSortable\SortableLink;
use BadMethodCallException;

trait SortableTrait
{
	use Sortable;

	/**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array                              $sortParameters
     *
     * @return \Illuminate\Database\Query\Builder
     *
     * @throws ColumnSortableException
     */
    private function queryOrderBuilder($query, array $sortParameters)
    {
        $model = $this;
        
        list($column, $direction) = $this->parseParameters($sortParameters);
        
        if (is_null($column)) {
            return $query;
        }
        
        $explodeResult = SortableLink::explodeSortParameter($column);
        if ( ! empty($explodeResult)) {
            $relationName = $explodeResult[0];
            $column       = $explodeResult[1];            
            try {
                $relation = $query->getRelation($relationName);
                $query    = $this->queryJoinBuilder($query, $relation);
            } catch (BadMethodCallException $e) {
                return $query;
            } catch (\Exception $e) {
                return $query;
            }

            $model = $relation->getRelated(); 
        }

        if (method_exists($model, camel_case($column).'Sortable')) {
            return call_user_func_array([$model, camel_case($column).'Sortable'], [$query, $direction]);
        }

        if (isset($model->sortableAs) && in_array($column, $model->sortableAs)) {
            $query = $query->orderBy($column, $direction);
        } elseif ($this->columnExists($model, $column)) { 
            $column = $model->getTable().'.'.$column;
            $query  = $query->orderBy($column, $direction);
        }

        return $query;
    }

}