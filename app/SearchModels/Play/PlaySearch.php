<?php

namespace App\SearchModels\Play;

use App\Play;
use App\SearchModels\{
	SearchTrait,
	SortableTrait,
	ValidateTrait
};

class PlaySearch extends Play
{
    use SearchTrait;
	use SortableTrait;
    use ValidateTrait;
    
    protected $table = 'plays';

    public $sortable = ['id', 'name', 'genre', 'duration', 'description', 'date', 'scene_type','status', 'created_at'];

    protected $rules = [
		'id' => 'integer|nullable',
		'name' => 'string|nullable',
		'genre' => 'string|nullable',
		'duration' => 'string|nullable',
		'scene_type' => 'integer|nullable',
		'status' => 'integer|nullable',
		'created_at' => 'date|nullable',
//		'date' => 'date|nullable', ХЗ тут же datetime нужно потом дoделать
    ];
    
    public function search($request)
	{	
		$querySearch = (new \ReflectionClass($this))->getShortName();	
		$fields = $request->query->get($querySearch, $default = []);		
		$query = Play::sortable()->with('playPhotos')->with('persons');			

		if(!$this->validate($fields, $this->rules)){
			return $query;
		}
      	
		return self::applyDecoratorsFromFields($fields, $query);
	}
}
