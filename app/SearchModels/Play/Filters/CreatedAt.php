<?php
namespace App\SearchModels\Play\Filters;

use Illuminate\Database\Eloquent\Builder;
use App\SearchModels\FilterInterface;


class CreatedAt implements FilterInterface
{
	 /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereDate('plays.created_at', '=', $value);
    }
}