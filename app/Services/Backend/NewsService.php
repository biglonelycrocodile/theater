<?php

namespace App\Services\Backend;

use App\SearchModels\News\NewsSearch;
use App\Http\Resources\Backend\NewsCollection;
use App\Http\Resources\Backend\NewsResource;
use App\News;

class NewsService
{
    public function getFiltredNews($request)
    {
        $newsSearch = new NewsSearch();
		$news = $newsSearch->search($request);		
		return new NewsCollection($news->paginate());
    }

    public function createNews($request)
    {
        $data = $request->only([
            'title','description', 'date', 'status','play_id',
        ]);        
        $news = News::create($data);
        return new NewsResource($news);        
    }

    public function getNews($id)
    {
        $news = News::where('id', $id)->firstOrFail();
		return new NewsResource($news);
    }

    public function updateNews($request, $id)
    {        
        $news = News::where('id', $id)->firstOrFail();        
        $data = $request->only([
            'title','description', 'date', 'status', 'play_id'
        ]); 
        $news->fill($data)->save();
        return new NewsResource($news);        
    }

    public function deleteNews($id)
    {
        $news = News::where('id', $id)->firstOrFail();
        $news->delete();
    }
}