<?php

namespace App\Services\Backend;

use App\SearchModels\Gallery\GallerySearch;
use App\Http\Resources\Backend\GalleryCollection;
use App\Http\Resources\Backend\GalleryResource;
use App\Gallery;

class GalleryService
{
    public function getFiltredGallery($request)
    {
        $gallerySearch = new GallerySearch();
		$gallery = $gallerySearch->search($request);		
		return new GalleryCollection($gallery->paginate());
    }

    public function createGallery($request)
    {
        $data = $request->only([
            'title','description', 'date', 'memorial_images', 'gallery_category_id'
        ]);        
        $gallery = Gallery::create($data);
        return new GalleryResource($gallery);        
    }

    public function getGallery($id)
    {
        $gallery = Gallery::where('id', $id)->firstOrFail();
		return new GalleryResource($gallery);
    }

    public function updateGallery($request, $id)
    {        
        $gallery = Gallery::where('id', $id)->firstOrFail();        
        $data = $request->only([
            'title','description', 'date', 'memorial_images', 'gallery_category_id'
        ]); 
        $gallery->fill($data)->save();
        return new GalleryResource($gallery);        
    }

    public function deleteGallery($id)
    {
        $gallery = Gallery::where('id', $id)->firstOrFail();
        $gallery->delete();
    }
}