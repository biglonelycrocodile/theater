<?php

namespace App\Services\Backend;

use App\SearchModels\GalleryCategory\GalleryCategorySearch;
use App\Http\Resources\Backend\GalleryCategoryCollection;
use App\Http\Resources\Backend\GalleryCategoryResource;
use App\GalleryCategory;

class GalleryCategoryService
{
    public function getFiltredGalleryCategory($request)
    {
        $gallerySearch = new GalleryCategorySearch();
		$gallery = $gallerySearch->search($request);		
		return new GalleryCategoryCollection($gallery->paginate());
    }

    public function createGalleryCategory($request)
    {
        $data = $request->only([
            'title'
        ]);        
        $gallery = GalleryCategory::create($data);
        return new GalleryCategoryResource($gallery);        
    }

    public function getGalleryCategory($id)
    {
        $gallery = GalleryCategory::where('id', $id)->firstOrFail();
		return new GalleryCategoryResource($gallery);
    }

    public function updateGalleryCategory($request, $id)
    {        
        $gallery = GalleryCategory::where('id', $id)->firstOrFail();        
        $data = $request->only([
            'title'
        ]); 
        $gallery->fill($data)->save();
        return new GalleryCategoryResource($gallery);        
    }

    public function deleteGalleryCategory($id)
    {
        $gallery = GalleryCategory::where('id', $id)->firstOrFail();
        $gallery->delete();
    }
}