<?php

namespace App\Services\Backend;

use App\SearchModels\Play\PlaySearch;
use App\Http\Resources\Backend\PlayCollection;
use App\Http\Resources\Backend\PlayResource;
use App\Play;

class PlayService
{
    public function getFiltredPlays($request)
    {
        $playSearch = new PlaySearch();
		$play = $playSearch->search($request);		
		return new PlayCollection($play->paginate());
    }

    public function createPlay($request)
    {
        $data = $request->only([
            'name', 'description', 
            'date', 'status'
        ]);        
        $play = Play::create($data);       
        return new PlayResource($play);        
    }


    public function getPlay($id)
	{
		$play = Play::where('id', $id)->firstOrFail();
		return new PlayResource($play);
    }
    
    public function updatePlay($request, $id)
    {
        $play = Play::where('id', $id)->firstOrFail();        
        $data = $request->only([
            'name', 'description', 
            'date', 'status'
        ]);         
        $play->fill($data)->save();       
        return new PlayResource($play);        
    }

    public function deletePlay($id)
    {
        $play = Play::where('id', $id)->firstOrFail();
        $play->delete();
    }
}