<?php

namespace App\Services\Backend;

use App\SearchModels\Person\PersonSearch;
use App\Http\Resources\Backend\PersonCollection;
use App\Http\Resources\Backend\PersonResource;
use App\Person;

class PersonService
{
    public function getFiltredPersons($request)
    {
        $personSearch = new PersonSearch();
		$person = $personSearch->search($request);		
		return new PersonCollection($person->paginate());
    }

    public function createPerson($request)
    {
        $data = $request->only([
            'first_name','last_name',  'description', 'status', 'memorial_images'           
        ]);        
        $person = Person::create($data);
        return new PersonResource($person); 
    }

    public function getPerson($id)
    {
        $person = Person::where('id', $id)->firstOrFail();
		return new PersonResource($person);
    }

    public function updatePerson($request, $id)
    {
        $person = Person::where('id', $id)->firstOrFail();        
        $data = $request->only([
            'first_name','last_name',  'description', 'status','memorial_images'           
        ]); 
        $person->fill($data)->save();
        return new PersonResource($person);        
    }

    public function deletePerson($id)
    {
        $person = Person::where('id', $id)->firstOrFail();
        $person->delete();
    }
}