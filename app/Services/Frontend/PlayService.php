<?php

namespace App\Services\Frontend;

use App\SearchModels\Play\PlaySearch;
use App\Http\Resources\Frontend\PlayCollection;
use App\Http\Resources\Frontend\PlayResource;
use App\Play;

class PlayService
{
    public function getFiltredPlays($request)
    {
        $playSearch = new PlaySearch();
		$play = $playSearch->search($request);		
		return new PlayCollection($play->paginate());
    }
    
    public function getPlay($id)
	{
		$play = Play::where('id', $id)->firstOrFail();
		return new PlayResource($play);
    }

}