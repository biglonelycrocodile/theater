<?php

namespace App\Services\Frontend;

use App\SearchModels\Gallery\GallerySearch;
use App\Http\Resources\Frontend\GalleryCollection;
use App\Http\Resources\Frontend\GalleryResource;
use App\Gallery;

class GalleryService
{
    public function getFiltredGallery($request)
    {
        $newsSearch = new GallerySearch();
		$news = $newsSearch->search($request);		
		return new GalleryCollection($news->paginate());
    }

    public function getGallery($id)
    {
        $news = Gallery::where('id', $id)->firstOrFail();
		return new GalleryResource($news);
    }
   
}