<?php

namespace App\Services\Frontend;

use Illuminate\Support\Facades\DB;
use App\Http\Resources\Frontend\SearchCollection;

class SearchService
{
    public function getSearchModels($request)
    {
        $search =  $request->route('q');
        $news = $this->getNews($search);
        $plays = $this->getPlays($search);
        $persons = $this->getPersons($search);
        $result = $persons
                ->union($news)
                ->union($plays)
                ->paginate();

        return new SearchCollection($result);
       
    }

    protected function getNews($search)
    {
        return DB::table('news')
            ->select(
                "id",
                "title",
                "description",
                DB::raw("('news') as 'type'")
            )
            ->where(function ($q) use ($search) {
                $q->where('title', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
    }

    protected function getPlays($search)
    {
        return DB::table('plays')
            ->select(
                "id",
                "name",
                "description",
                DB::raw("('plays') as 'type'")
            )->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
    }

    protected function getPersons($search)
    {
        return DB::table('persons')
            ->select(
                "id",
                DB::raw("CONCAT(first_name, ' ', last_name) as title"),
                "description",
                DB::raw("('persons') as 'type'")
            )->where(function ($q) use ($search) {
                $q->where(DB::raw("CONCAT(first_name, ' ', last_name)"), 'LIKE', '%' . $search . '%')
                    ->orWhere(DB::raw("CONCAT(last_name, ' ', first_name)"), 'LIKE', '%' . $search . '%');
            });
    }
}
