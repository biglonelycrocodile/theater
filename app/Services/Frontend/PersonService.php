<?php

namespace App\Services\Frontend;

use App\SearchModels\Person\PersonSearch;
use App\Http\Resources\Frontend\PersonCollection;
use App\Http\Resources\Frontend\PersonResource;
use App\Person;

class PersonService
{
    public function getFiltredPersons($request)
    {
        $personSearch = new PersonSearch();
		$person = $personSearch->search($request);		
		return new PersonCollection($person->paginate());
    }
   
    public function getPerson($id)
    {
        $person = Person::where('id', $id)->firstOrFail();
		return new PersonResource($person);
    }
    
}