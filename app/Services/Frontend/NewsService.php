<?php

namespace App\Services\Frontend;

use App\SearchModels\News\NewsSearch;
use App\SearchModels\News\AfishaNewsSearch;
use App\Http\Resources\Frontend\NewsCollection;
use App\Http\Resources\Frontend\NewsResource;
use App\News;

class NewsService
{
    public function getFiltredNews($request)
    {
        $newsSearch = new NewsSearch();
		$news = $newsSearch->search($request);		
		return new NewsCollection($news->paginate());
    }

    public function getNews($id)
    {
        $news = News::where('id', $id)->firstOrFail();
		return new NewsResource($news);
    }

    public function getFiltredAfishaNews($request)
    {
        $newsSearch = new AfishaNewsSearch();
		$news = $newsSearch->search($request);		
		return new NewsCollection($news->paginate());
    }
    
   
}