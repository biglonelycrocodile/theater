<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 2;

    protected $fillable = [
        'title', 'description', 'date', 'status', 'base_url', 'path', 'play_id'
    ];

    protected $dates = ['deleted_at'];

    public $fileUpload = [                   
        'field' => 'news_photo'         
    ];

    public $fileUploads = [
        [
            'field' => 'news_photos',
            'uploadRelation' => 'newsPhotos',
            'multiple' => true
        ]        
    ];

    public function scopeActive($query)
    {
        return $query->where('news.status', self::STATUS_ACTIVE);
    }

    public static function getStatuses()
    {
        $statuses =  [
            self::STATUS_NOT_ACTIVE => __('messages.not_active'),
            self::STATUS_ACTIVE => __('messages.active'),           
        ];        
        return $statuses;
    }

    public static function getStatus($status)
    {
        $statuses = self::getStatuses();
        return ($statuses[$status]) ?? __('messages.no_set');
    }

    public function newsPhotos()
    {
        return $this->hasMany('App\NewsPhoto', 'news_id');
    }

    public function getImage()
    {
        return $this->base_url . $this->path;
    }

    public function play()
    {
        return $this->belongsTo('App\Play', 'play_id');
    }
}
