<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title'
    ];
   
    public function galleries()
    {
        return $this->hasMany('App\Gallery', 'gallery_category_id');
    }
}
