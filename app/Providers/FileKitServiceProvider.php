<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\FileKitObserver;
use App\Play;
use App\Person;
use App\News;
use App\Gallery;

class FileKitServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {   
        Play::observe(FileKitObserver::class);
        Person::observe(FileKitObserver::class);
        News::observe(FileKitObserver::class);
        Gallery::observe(FileKitObserver::class);
    }
}
