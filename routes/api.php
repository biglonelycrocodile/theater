<?php

use Illuminate\Http\Request;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['role:' . User::ROLE_ADMINISTRATOR, 'auth:api', 'cors'])->group(function () {	
    Route::apiResource('/v1/backend/play', 'Backend\PlayController');
    Route::apiResource('/v1/backend/person', 'Backend\PersonController');
    Route::apiResource('/v1/backend/news', 'Backend\NewsController');
    Route::apiResource('/v1/backend/gallery', 'Backend\GalleryController');
    Route::apiResource('/v1/backend/gallery-category', 'Backend\GalleryCategoryController');
});


Route::middleware(['cors'])->group(function () {	
    Route::get('/v1/news/afisha',['as' => 'afisha', 'uses' => 'Frontend\NewsController@afisha']);
    
    Route::apiResource('/v1/play', 'Frontend\PlayController');
    Route::apiResource('/v1/person', 'Frontend\PersonController');
    Route::apiResource('/v1/news', 'Frontend\NewsController');
    Route::apiResource('/v1/gallery', 'Frontend\GalleryController');
    
    Route::get('/v1/search/{q}',['as' => 'search', 'uses' => 'Frontend\SearchController@index']);
    
});

Route::post('/v1/backend/login',['as' => 'backend.auth.login', 'uses' => 'Backend\Auth\LoginController@login'])->middleware('cors');
Route::post('/v1/backend/play/test', ['as' => 'play.test', 'uses' =>'Backend\PlayController@test'])->middleware('cors');

